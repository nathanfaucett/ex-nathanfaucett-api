defmodule NathanFaucett.Web.Controller.UserTest do
  use NathanFaucett.Web.ConnCase

  alias NathanFaucett.Service.User.Create

  @identity_attrs %{
    email: "email",
    password: "password"
  }

  def fixture(:user) do
    {:ok, user} = Create.call(@identity_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "sign_in" do
    test "signs user in", %{conn: conn} do
      _user = fixture(:user)
      conn = post(conn, Routes.auth_path(conn, :callback, :identity, %{user: @identity_attrs}))

      assert %{
               "id" => id,
               "email" => "email"
             } = json_response(conn, 200)["data"]
    end
  end
end
