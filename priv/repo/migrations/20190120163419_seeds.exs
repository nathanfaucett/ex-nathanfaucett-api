defmodule NathanFaucett.Repo.Migrations.InitialSeeds do
  use Ecto.Migration
  import Ecto.Changeset

  def up do
    NathanFaucett.Repo.transaction(fn ->
      {:ok, _user} =
        NathanFaucett.Service.User.Create.call(%{
          email: System.get_env("EMAIL"),
          password: System.get_env("PASSWORD")
        })
    end)
  end
end
