defmodule NathanFaucett.Web.Plug.Authentication do
  use NathanFaucett.Web, :plugs

  alias NathanFaucett.Web.Guardian
  alias NathanFaucett.Model.User

  def init(opts), do: opts

  def call(conn, _opts) do
    token = get_req_header(conn, "authorization") |> List.first()
    authorize_connection(conn, token)
  end

  defp unauthorized(conn) do
    conn
    |> put_status(401)
    |> render(NathanFaucett.Web.ErrorView, "401.json")
    |> halt()
  end

  defp authorize_connection(conn, nil), do: unauthorized(conn)

  defp authorize_connection(conn, token) do
    case Guardian.decode_and_verify(token) do
      {:ok, %{"sub" => user_id}} ->
        assign_auth_data(conn, token, User.get(user_id))

      _otherwise ->
        unauthorized(conn)
    end
  end

  defp assign_auth_data(conn, _token, nil), do: unauthorized(conn)

  defp assign_auth_data(conn, token, %User{} = user) do
    conn
    |> assign(:token, token)
    |> assign(:user, user)
  end
end
