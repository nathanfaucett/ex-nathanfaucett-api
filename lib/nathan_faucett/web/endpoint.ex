defmodule NathanFaucett.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :nathan_faucett

  socket "/socket", NathanFaucett.Web.Channel.User,
    websocket: true,
    longpoll: false

  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug CORSPlug

  plug NathanFaucett.Web.Router
end
