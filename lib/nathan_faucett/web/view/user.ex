defmodule NathanFaucett.Web.View.User do
  use NathanFaucett.Web, :view
  alias NathanFaucett.Web.View.User

  def render("show.json", %{user: user, token: token}) do
    %{data: render_one(user, User, "user.json", %{token: token})}
  end

  def render("user.json", %{user: user, token: token}) do
    %{
      id: user.id,
      token: token,
      email: user.email,
      inserted_at: user.inserted_at,
      updated_at: user.updated_at
    }
  end
end
