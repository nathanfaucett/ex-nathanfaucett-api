defmodule NathanFaucett.Web.View.Error do
  use NathanFaucett.Web, :view

  alias Ecto.Changeset
  import NathanFaucett.Web.View.ErrorHelpers, only: [translate_error: 1]

  def render("command_handler_error.json", %{failed_op: _op, failed_value: %Ecto.Changeset{} = cs}) do
    %{
      errors: Changeset.traverse_errors(cs, &translate_error/1)
    }
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end
end
