defmodule NathanFaucett.Web.View.Post do
  use NathanFaucett.Web, :view
  alias NathanFaucett.Web.View.Post

  def render("index.json", %{posts: posts}) do
    %{data: render_many(posts, Post, "post.json")}
  end

  def render("show.json", %{post: post}) do
    %{data: render_one(post, Post, "post.json")}
  end

  def render("post.json", %{post: post}) do
    %{
      id: post.id,
      title: post.title,
      body: post.body,
      inserted_at: post.inserted_at,
      updated_at: post.updated_at
    }
  end
end
