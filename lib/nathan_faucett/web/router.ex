defmodule NathanFaucett.Web.Router do
  use NathanFaucett.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug(NathanFaucett.Web.Plug.Authentication)
  end

  scope "/", NathanFaucett.Web.Controller do
    pipe_through :api

    scope "/auth" do
      pipe_through :authenticated

      delete("/sign_out", Auth, :sign_out)
      get("/current_user", Auth, :current_user)
    end

    scope "/auth" do
      get("/:provider", Auth, :request)

      get("/:provider/callback", Auth, :callback)
      post("/:provider/callback", Auth, :callback)
      post("/identity/callback", Auth, :identity_callback)
    end

    scope "/posts" do
      resources "/", Post, only: [:index, :show]

      pipe_through :authenticated

      resources "/", Post, only: [:create, :update, :delete]
    end
  end
end
