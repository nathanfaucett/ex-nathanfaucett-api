defmodule NathanFaucett.Web.Controller.Auth do
  use NathanFaucett.Web, :controller

  alias NathanFaucett.Web.Guardian
  alias NathanFaucett.Service.User.SignIn

  plug(Ueberauth)

  action_fallback(NathanFaucett.Web.Controller.Fallback)

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_status(401)
    |> render(NathanFaucett.Web.View.Error, "401.json")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    case sign_in(auth) do
      {:ok, user} ->
        conn = Guardian.Plug.sign_in(conn, user.id)
        token = Guardian.Plug.current_token(conn)

        conn
        |> put_resp_header("authorization", token)
        |> put_view(NathanFaucett.Web.View.User)
        |> render("show.json", user: user, token: token)

      {:error, _reason} ->
        conn
        |> put_status(500)
        |> render(NathanFaucett.Web.View.Error, "500.json")
    end
  end

  defp sign_in(%Ueberauth.Auth{provider: :identity, credentials: credentials} = auth) do
    SignIn.call(%{email: auth.info.email, password: credentials.other.password})
  end

  def identity_callback(%{assigns: %{ueberauth_auth: _auth}} = conn, params) do
    callback(conn, params)
  end

  def current_user(conn, _params) do
    user = conn.assigns[:user]
    token = conn.assigns[:token]

    conn
    |> put_view(NathanFaucett.Web.View.User)
    |> render("show.json", user: user, token: token)
  end

  def sign_out(conn, _params) do
    token =
      conn
      |> get_req_header("authorization")
      |> List.first()

    with {:ok, _claims} <- NathanFaucett.Web.Guardian.revoke(token) do
      send_resp(conn, :no_content, "")
    end
  end
end
