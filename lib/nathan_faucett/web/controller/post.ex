defmodule NathanFaucett.Web.Controller.Post do
  use NathanFaucett.Web, :controller

  alias NathanFaucett.Service.Post.{Index, Show, Create, Update, Delete}

  action_fallback(NathanFaucett.Web.Controller.Fallback)

  def index(conn, _params) do
    with {:ok, posts} <- Index.call() do
      conn
      |> put_view(NathanFaucett.Web.View.Post)
      |> render("index.json", posts: posts)
    end
  end

  def index(conn, %{ "page" => page, "count" => count }) do
    with {:ok, posts} <- Index.call(%{page: page, count: count}) do
      conn
      |> put_view(NathanFaucett.Web.View.Post)
      |> render("index.json", posts: posts)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, post} <- Show.call(%{id: id}) do
      conn
      |> put_view(NathanFaucett.Web.View.Post)
      |> render("show.json", post: post)
    end
  end

  def create(conn, %{"post" => post_params}) do
    with {:ok, post} <- Create.call(%{post: post_params}) do
      conn
      |> put_status(:created)
      |> put_view(NathanFaucett.Web.View.Post)
      |> put_resp_header("location", Routes.post_path(conn, :show, post))
      |> render("show.json", post: post)
    end
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    with {:ok, post} <- Update.call(%{id: id, post: post_params}) do
      conn
      |> put_view(NathanFaucett.Web.View.Post)
      |> render("show.json", post: post)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, _post} <- Delete.call(%{id: id}) do
      send_resp(conn, :no_content, "")
    end
  end
end
