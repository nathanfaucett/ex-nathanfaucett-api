defmodule NathanFaucett.Web.Controller.Fallback do
  use NathanFaucett.Web, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(NathanFaucett.Web.View.Changeset)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(NathanFaucett.Web.View.Error)
    |> render("404.json")
  end

  def call(conn, {:error, error}) do
    conn
    |> put_status(422)
    |> json(%{data: error})
  end
end
