defmodule NathanFaucett.Web do
  def controller do
    quote do
      use Phoenix.Controller, namespace: NathanFaucett.Web

      import Plug.Conn
      import NathanFaucett.Gettext
      alias NathanFaucett.Web.Router.Helpers, as: Routes
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/nathan_faucett_web/templates",
        namespace: NathanFaucett.Web

      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      import NathanFaucett.Web.View.ErrorHelpers
      import NathanFaucett.Gettext
      alias NathanFaucett.Web.Router.Helpers, as: Routes
    end
  end

  def plugs do
    quote do
      use Phoenix.Controller, namespace: StembordWeb
      import Plug.Conn
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import NathanFaucett.Gettext
    end
  end

  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
