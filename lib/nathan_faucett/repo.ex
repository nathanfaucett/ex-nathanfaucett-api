defmodule NathanFaucett.Repo do
  use Ecto.Repo,
    otp_app: :nathan_faucett,
    adapter: Ecto.Adapters.Postgres
end
