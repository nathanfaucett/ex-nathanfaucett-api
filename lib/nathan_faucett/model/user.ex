defmodule NathanFaucett.Model.User do
  use Ecto.Schema

  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias NathanFaucett.Model.User
  alias NathanFaucett.Repo

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :encrypted_password, :string

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :encrypted_password])
    |> validate_required([:email, :encrypted_password])
  end

  def to_json(%User{} = user) do
    Map.take(user, [:id, :body, :title, :inserted_at, :updated_at])
  end

  def all do
    Repo.all(User)
  end

  def get(id), do: Repo.get(User, id)
  def get!(id), do: Repo.get!(User, id)
  def get_by(search), do: Repo.get_by(User, search)
  def get_by!(search), do: Repo.get_by!(User, search)

  def create(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def create!(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert!()
  end

  def update(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def update!(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update!()
  end

  def delete(%User{} = user) do
    Repo.delete(user)
  end

  def delete!(%User{} = user) do
    Repo.delete!(user)
  end

  def change(%User{} = user) do
    User.changeset(user, %{})
  end
end
