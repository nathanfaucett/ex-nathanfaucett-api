defmodule NathanFaucett.Model.Post do
  use Ecto.Schema

  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias NathanFaucett.Model.Post
  alias NathanFaucett.Repo

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "posts" do
    field :body, :string
    field :title, :string

    timestamps()
  end

  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :body])
    |> validate_required([:title, :body])
  end

  def to_json(%Post{} = post) do
    Map.take(post, [:id, :body, :title, :inserted_at, :updated_at])
  end

  def all(%{page: page, count: count}) do
    from(p in Post,
      limit: ^count,
      offset: ^page,
      order_by: [asc: p.updated_at]
    )
    |> Repo.all()
  end

  def all do
    Repo.all(Post)
  end

  def get(id), do: Repo.get(Post, id)
  def get!(id), do: Repo.get!(Post, id)
  def get_by(search), do: Repo.get_by(Post, search)
  def get_by!(search), do: Repo.get_by!(Post, search)

  def create(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  def create!(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert!()
  end

  def update(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  def update!(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update!()
  end

  def delete(%Post{} = post) do
    Repo.delete(post)
  end

  def delete!(%Post{} = post) do
    Repo.delete!(post)
  end

  def change(%Post{} = post) do
    Post.changeset(post, %{})
  end
end
