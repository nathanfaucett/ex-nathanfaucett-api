defmodule NathanFaucett.Service.Post.Update do
  alias NathanFaucett.Model.Post

  def call(%{id: id, post: post_attrs}) do
    case Post.get(id) do
      nil ->
        {:error, :not_found}

      post ->
        Post.update(post, post_attrs)
    end
  end
end
