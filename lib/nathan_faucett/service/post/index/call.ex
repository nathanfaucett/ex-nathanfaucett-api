defmodule NathanFaucett.Service.Post.Index do
  alias NathanFaucett.Model.Post

  def call(%{} = params) do
    {:ok, Post.all(params)}
  end

  def call() do
    {:ok, Post.all()}
  end
end
