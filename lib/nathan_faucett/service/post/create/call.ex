defmodule NathanFaucett.Service.Post.Create do
  alias NathanFaucett.Model.Post

  def call(%{post: post_attrs}) do
    Post.create(post_attrs)
  end
end
