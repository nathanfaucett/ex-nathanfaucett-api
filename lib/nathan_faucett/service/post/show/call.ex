defmodule NathanFaucett.Service.Post.Show do
  alias NathanFaucett.Model.Post

  def call(%{id: id} = _attrs) do
    case Post.get(id) do
      nil ->
        {:error, :not_found}

      user ->
        {:ok, user}
    end
  end
end
