defmodule NathanFaucett.Service.Post.Delete do
  alias NathanFaucett.Model.Post

  def call(%{id: id}) do
    NathanFaucett.Repo.transaction(fn ->
      post = Post.get!(id)
      Post.delete!(post)
    end)
  end
end
