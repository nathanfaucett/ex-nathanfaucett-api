defmodule NathanFaucett.Service.User.SignIn do
  alias NathanFaucett.Model.User

  def call(%{email: email, password: password} = _attrs) do
    user = User.get_by(email: email)

    if user != nil and Argon2.check_pass(user.encrypted_password, password) do
      {:ok, user}
    else
      {:error, :not_found}
    end
  end
end
