defmodule NathanFaucett.Service.User.Create do
  alias NathanFaucett.Model.User

  def call(%{email: email, password: password} = _attrs) do
    NathanFaucett.Repo.transaction(fn ->
      %{password_hash: encrypted_password} = Argon2.add_hash(password)
      User.create!(%{email: email, encrypted_password: encrypted_password})
    end)
  end
end
