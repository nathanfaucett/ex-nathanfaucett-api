# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :nathan_faucett,
  ecto_repos: [NathanFaucett.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :nathan_faucett, NathanFaucett.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wF2JEGZt8htPRCCWxuDaB05lZDEhg8pIH72uX3LUsEW9JhnyBtOljer7JN4wyEdY",
  render_errors: [view: NathanFaucett.Web.View.Error, accepts: ~w(json)],
  pubsub: [name: NathanFaucett.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :nathan_faucett, NathanFaucett.Repo,
  username: "postgres",
  password: "postgres",
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 10

config :ueberauth, Ueberauth,
  providers: [
    identity: {Ueberauth.Strategy.Identity, [callback_methods: ["POST"], param_nesting: "user"]}
  ]

config :nathan_faucett, NathanFaucett.Web.Guardian,
  issuer: "nathan_faucett",
  secret_key: System.get_env("GUARDIAN_SECRET")

config :cors_plug,
  origin: ~r/.*/,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
