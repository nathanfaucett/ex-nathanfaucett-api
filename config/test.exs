use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :nathan_faucett, NathanFaucett.Web.Endpoint,
  http: [port: 4002],
  server: false,
  ui_url: "http://localhost:1234"

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :nathan_faucett, NathanFaucett.Repo,
  database: "nathan_faucett_test",
  pool: Ecto.Adapters.SQL.Sandbox
