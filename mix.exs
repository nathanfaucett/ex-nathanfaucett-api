defmodule NathanFaucett.MixProject do
  use Mix.Project

  def name do
    :nathan_faucett
  end

  def version do
    "0.1.0"
  end

  def project do
    [
      app: name(),
      version: version(),
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {NathanFaucett.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:cors_plug, "~> 2.0"},

      # Authentication
      {:comeonin, "~> 5.1"},
      {:argon2_elixir, "~> 2.0"},
      {:guardian, "~> 1.2"},
      {:ueberauth, "~> 0.5"},
      {:ueberauth_identity, "~> 0.2"},
      {:plug_cowboy, "~> 2.0"},

      # Deployment
      {:distillery, "~> 2.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      postgres: [
        "cmd mkdir -p ${PWD}/.volumes/#{name()}-postgres",
        "cmd docker run --rm -d " <>
          "--name #{name()}-postgres " <>
          "-e POSTGRES_PASSWORD=postgres " <>
          "-p 5432:5432 " <>
          "-v ${PWD}/.volumes/#{name()}-postgres:/var/lib/postgresql/data " <>
          "postgres:11"
      ],
      "postgres.delete": [
        "cmd docker rm -f #{name()}-postgres",
        "cmd rm -rf ${PWD}/.volumes"
      ],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
