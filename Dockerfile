FROM elixir:1.8

RUN apt-get update
RUN apt-get install -y postgresql-client
RUN apt-get autoclean

ARG MIX_ENV=prod
ENV MIX_ENV=${MIX_ENV}

COPY _build/${MIX_ENV}/rel /app/rel
COPY entrypoint.sh /app/entrypoint.sh
WORKDIR /app

ENTRYPOINT /app/entrypoint.sh